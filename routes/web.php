<?php

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login')->name('login.attempt')->uses('Auth\LoginController@login');

Route::group(['middleware' => 'auth'], function () {
    Route::resource('/', 'DashController');
    Route::get('/logout')->name('logout')->uses('Auth\LoginController@logout');


    //Ruta para mandar el formulario
    Route::post('/', 'CitasController@store');

    //Ruta para ver formulario de citas
    Route::get('/create', 'CitasController@index');










    //ver la pagina donde estan todos los usuarios
    Route::get('/users', 'UserController@index');


    //mostrar el formulario de usuario nuevo
    Route::get('/users/create', 'UserController@create');

    //guardar usuario nuevo
    Route::post('/users', 'UserController@store');

    //mostrar el formulario de actualizar usuario

    Route::get('/users{id}/edit', 'UsersController@edit');

    //guardar el usuario actualizado
    Route::put('/users/{id}', 'UserController@update');

    //eliminar un usuario en especifico
    Route::delete('/users/{id}', 'UserController@destroy');

});

