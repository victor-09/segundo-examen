(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[9],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Citas/Create.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Pages/Citas/Create.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Shared_Layout__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../Shared/Layout */ "./resources/js/Shared/Layout.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      registerDialogVisible: false,
      reg_form: {
        nombrecliente: '',
        contacto: '',
        nombredemascota: '',
        tipodemascota: '',
        edaddemascota: '',
        propositodecita: '',
        pesodemascota: ''
      },
      reg_rules: {
        nombrecliente: {
          required: true,
          message: 'El nombre del cliente es requerido'
        },
        contacto: {
          required: true,
          message: 'El contacto del cliente es requerido'
        },
        nombredemascota: {
          required: true,
          message: 'El nombre de la mascota es requerido'
        },
        tipodemascota: {
          required: true,
          message: 'El tipo de mascota es requerido'
        },
        edaddemascota: {
          required: true,
          message: 'La edad de la mascota es requerido'
        },
        propositodecita: {
          required: true,
          message: 'El proposito de la cita es requerido'
        },
        pesodemascota: {
          required: true,
          message: 'El peso de la mascota es requerido'
        }
      }
    };
  },
  methods: {
    submit: function submit() {
      var _this = this;

      this.$refs.reg_form.validate(function () {
        _this.$inertia.post('/citas', {
          nombrecliente: _this.reg_form.nombrecliente,
          contacto: _this.reg_form.contacto,
          nombredemascota: _this.reg_form.nombredemascota,
          tipodemascota: _this.reg_form.tipodemascota,
          edaddemascota: _this.reg_form.edaddemascota,
          propositodecita: _this.reg_form.propositodecita,
          pesodemascota: _this.reg_form.pesodemascota
        });
      });
    }
  },
  mounted: function mounted() {
    if (this.$page.citas) {
      this.citas = this.$page.citas;
    }
  },
  components: {
    Layout: _Shared_Layout__WEBPACK_IMPORTED_MODULE_0__["default"]
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Shared/Layout.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Shared/Layout.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_avatar__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-avatar */ "./node_modules/vue-avatar/dist/vue-avatar.min.js");
/* harmony import */ var vue_avatar__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue_avatar__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'layout',
  components: {
    Avatar: vue_avatar__WEBPACK_IMPORTED_MODULE_0___default.a
  },
  data: function data() {
    return {
      name: ''
    };
  },
  methods: {},
  mounted: function mounted() {
    if (this.$page.prop.auth.username) {
      this.name = this.$page.prop.auth.user.username;
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Citas/Create.vue?vue&type=template&id=69e24534&":
/*!**********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Pages/Citas/Create.vue?vue&type=template&id=69e24534& ***!
  \**********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "layout",
    [
      _c(
        "el-card",
        { staticClass: "el-card" },
        [
          _c(
            "el-form",
            {
              ref: "reg_form",
              attrs: { model: _vm.reg_form, rules: _vm.reg_rules }
            },
            [
              _c(
                "el-form-item",
                {
                  attrs: { label: "Nombre del Cliente", prop: "nombrecliente" }
                },
                [
                  _c("el-input", {
                    attrs: { placeholder: "Nombre del Cliente" },
                    model: {
                      value: _vm.reg_form.nombrecliente,
                      callback: function($$v) {
                        _vm.$set(_vm.reg_form, "nombrecliente", $$v)
                      },
                      expression: "reg_form.nombrecliente"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-form-item",
                { attrs: { label: "Contacto", prop: "contacto" } },
                [
                  _c("el-input", {
                    attrs: { placeholder: "Contacto" },
                    model: {
                      value: _vm.reg_form.contacto,
                      callback: function($$v) {
                        _vm.$set(_vm.reg_form, "contacto", $$v)
                      },
                      expression: "reg_form.contacto"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-form-item",
                {
                  attrs: { label: "Nombre de Mascota", prop: "nombredemascota" }
                },
                [
                  _c("el-input", {
                    attrs: { placeholder: "Nombre de Mascota" },
                    model: {
                      value: _vm.reg_form.nombredemascota,
                      callback: function($$v) {
                        _vm.$set(_vm.reg_form, "nombredemascota", $$v)
                      },
                      expression: "reg_form.nombredemascota"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-form-item",
                { attrs: { label: "Tipo de Mascota", prop: "tipodemascota" } },
                [
                  _c("el-input", {
                    attrs: { placeholder: "Tipo de Mascota" },
                    model: {
                      value: _vm.reg_form.tipodemascota,
                      callback: function($$v) {
                        _vm.$set(_vm.reg_form, "tipodemascota", $$v)
                      },
                      expression: "reg_form.tipodemascota"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-form-item",
                { attrs: { label: "Edad de Mascota", prop: "edaddemascota" } },
                [
                  _c("el-input", {
                    attrs: { placeholder: "Edad de Mascota" },
                    model: {
                      value: _vm.reg_form.edaddemascota,
                      callback: function($$v) {
                        _vm.$set(_vm.reg_form, "edaddemascota", $$v)
                      },
                      expression: "reg_form.edaddemascota"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-form-item",
                {
                  attrs: { label: "Proposito de Cita", prop: "propositodecita" }
                },
                [
                  _c("el-input", {
                    attrs: { placeholder: "Proposito de Cita" },
                    model: {
                      value: _vm.reg_form.propositodecita,
                      callback: function($$v) {
                        _vm.$set(_vm.reg_form, "propositodecita", $$v)
                      },
                      expression: "reg_form.propositodecita"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-form-item",
                { attrs: { label: "Peso de Mascota", prop: "pesodemascota" } },
                [
                  _c("el-input", {
                    attrs: { placeholder: "Peso de Mascota" },
                    model: {
                      value: _vm.reg_form.pesodemascota,
                      callback: function($$v) {
                        _vm.$set(_vm.reg_form, "pesodemascota", $$v)
                      },
                      expression: "reg_form.pesodemascota"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _vm.$page.prop.errors
                ? _c(
                    "div",
                    _vm._l(_vm.$page.prop.errors, function(error) {
                      return _c(
                        "div",
                        _vm._l(error, function(er, index) {
                          return _c("el-alert", {
                            key: index,
                            staticStyle: { "margin-bottom": "5px" },
                            attrs: { type: "error", title: er, closable: false }
                          })
                        }),
                        1
                      )
                    }),
                    0
                  )
                : _vm._e(),
              _vm._v(" "),
              _c(
                "div",
                { staticStyle: { "text-align": "center" } },
                [
                  _c(
                    "el-button",
                    {
                      attrs: { type: "primary" },
                      on: {
                        click: function($event) {
                          return _vm.submit()
                        }
                      }
                    },
                    [_vm._v("Registrar")]
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Shared/Layout.vue?vue&type=template&id=6bf30086&":
/*!*****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Shared/Layout.vue?vue&type=template&id=6bf30086& ***!
  \*****************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "el-container",
    { staticStyle: { height: "100%" } },
    [
      _c(
        "el-header",
        { staticStyle: { "text-align": "right" } },
        [
          _c(
            "el-dropdown",
            {
              staticStyle: { "margin-top": "10px" },
              attrs: { trigger: "click" }
            },
            [
              _c("avatar", {
                attrs: {
                  username: "name",
                  size: 40,
                  "background-color": "#70d69d"
                }
              }),
              _vm._v(" "),
              _c(
                "el-dropdown-menu",
                { attrs: { slot: "dropdown" }, slot: "dropdown" },
                [
                  _c(
                    "a",
                    { attrs: { href: "/logout" } },
                    [_c("el-dropdown-item", [_vm._v("Salir")])],
                    1
                  )
                ]
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "el-container",
        [
          _c(
            "el-menu",
            { attrs: { collapse: "" } },
            [
              _c(
                "inertia-link",
                { attrs: { href: "/" } },
                [
                  _c("el-menu-item", { attrs: { index: "1" } }, [
                    _c("i", { staticClass: "fas fa-home" }),
                    _vm._v(" "),
                    _c("span", { attrs: { slot: "title" }, slot: "title" }, [
                      _vm._v("Dashboard")
                    ])
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "inertia-link",
                { attrs: { href: "/users" } },
                [
                  _c("el-menu-item", { attrs: { index: "2" } }, [
                    _c("i", { staticClass: "fas fa-users" }),
                    _vm._v(" "),
                    _c("span", { attrs: { slot: "title" }, slot: "title" }, [
                      _vm._v("Usuario")
                    ])
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "inertia-link",
                { attrs: { href: "/citas" } },
                [
                  _c("el-menu-item", { attrs: { index: "3" } }, [
                    _c("i", { staticClass: "fas fa-users" }),
                    _vm._v(" "),
                    _c("span", { attrs: { slot: "title" }, slot: "title" }, [
                      _vm._v("Citas")
                    ])
                  ])
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c("el-main", [_vm._t("default")], 2)
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/Pages/Citas/Create.vue":
/*!*********************************************!*\
  !*** ./resources/js/Pages/Citas/Create.vue ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Create_vue_vue_type_template_id_69e24534___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Create.vue?vue&type=template&id=69e24534& */ "./resources/js/Pages/Citas/Create.vue?vue&type=template&id=69e24534&");
/* harmony import */ var _Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Create.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Citas/Create.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Create_vue_vue_type_template_id_69e24534___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Create_vue_vue_type_template_id_69e24534___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Citas/Create.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Citas/Create.vue?vue&type=script&lang=js&":
/*!**********************************************************************!*\
  !*** ./resources/js/Pages/Citas/Create.vue?vue&type=script&lang=js& ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Create.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Citas/Create.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Pages/Citas/Create.vue?vue&type=template&id=69e24534&":
/*!****************************************************************************!*\
  !*** ./resources/js/Pages/Citas/Create.vue?vue&type=template&id=69e24534& ***!
  \****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_69e24534___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Create.vue?vue&type=template&id=69e24534& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Citas/Create.vue?vue&type=template&id=69e24534&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_69e24534___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_69e24534___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/Shared/Layout.vue":
/*!****************************************!*\
  !*** ./resources/js/Shared/Layout.vue ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Layout_vue_vue_type_template_id_6bf30086___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Layout.vue?vue&type=template&id=6bf30086& */ "./resources/js/Shared/Layout.vue?vue&type=template&id=6bf30086&");
/* harmony import */ var _Layout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Layout.vue?vue&type=script&lang=js& */ "./resources/js/Shared/Layout.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Layout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Layout_vue_vue_type_template_id_6bf30086___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Layout_vue_vue_type_template_id_6bf30086___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Shared/Layout.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/Shared/Layout.vue?vue&type=script&lang=js&":
/*!*****************************************************************!*\
  !*** ./resources/js/Shared/Layout.vue?vue&type=script&lang=js& ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Layout.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Shared/Layout.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Shared/Layout.vue?vue&type=template&id=6bf30086&":
/*!***********************************************************************!*\
  !*** ./resources/js/Shared/Layout.vue?vue&type=template&id=6bf30086& ***!
  \***********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_template_id_6bf30086___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Layout.vue?vue&type=template&id=6bf30086& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Shared/Layout.vue?vue&type=template&id=6bf30086&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_template_id_6bf30086___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_template_id_6bf30086___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);