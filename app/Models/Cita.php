<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cita extends Model
{
    protected $fillable = ["nombrecliente", "nombremascota", "contactocliente", "tipomascota", "edadmascota", "propositocita", "pesomascota"];
}
